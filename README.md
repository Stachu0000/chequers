Warcaby

- gracz - AI,
- gracz rozpoczyna grę pionkami na dole "O",
- gra się dopóki nie zostaną zbite wszystkie pionki gracza lub AI,
	lub jeżeli nie któraś ze stron nie bezie mieć możliwości ruchu,
- bicia są obowiązkowe, program wymusza na graczu wykonanie ruchu 
	pionkiem mającym bicie a nie pionkiem bez bicia,
- bicia podwójne mają priorytet nad pojedyczymi
- gdy gracz wykona pojedyńcze bicie i będzie dalej możliwość bicia 
	to program wymusi kontynuowanie bicia do póki będzie to możliwe
- logika bicia jest taka sama dla AI
- damki nie zaimplementowano

