import java.util.*;

public class PlayerLogic {
    Scanner scanner = new Scanner(System.in);
    Board board;
    List<String> cellsIDs;
    Cell startCell;
    Cell endCell;
    int startRow;
    int endRow;
    int startColumn;
    int endColumn;
    boolean moveCompleted;
    List<Cell> cellsWithOneCaptureMoves = new ArrayList<>();
    List<Cell> cellsWithDoubleCaptureMoves = new ArrayList<>();


    public PlayerLogic(Board board, List<String> cellsIDs) {
        this.board = board;
        this.cellsIDs = cellsIDs;
    }

    public void playerMove() {
        moveCompleted = false;

        findCellsWithCaptureMoves();

        while (!moveCompleted) {
            getPlayerStartCell();

            if (!getPlayerEndCell()) {
                continue;
            }

            setupCellsParameters();

            validateDiagonalMove();
        }
    }

    public void getPlayerStartCell() {
        System.out.println("Twój ruch, Podaj adres pionka zaczynającego ruch.");

        while (true) {
            String cellId = scanner.nextLine().toUpperCase();
            String validatedId = validateCellId(cellId);
            startCell = getCellById(validatedId);

            if (!hasPlayerPawn(startCell)) {
                System.out.println("Na tym polu nie ma Twojego pionka, podaj poprawne pole.");
            }
            //If double capturing move is possible and Player starting Pawn doesn't have double capturing possibility, it will repeat loop
            else if (!cellsWithDoubleCaptureMoves.isEmpty() && !cellsWithDoubleCaptureMoves.contains(startCell)) {
                System.out.println("Inny pionek ma dostępne podwójne bicie, wybierz właściwe pole.");

            }
            //If single capturing move is possible and Player starting Pawn doesn't have any capturing possibility, it will repeat loop
            else if (!cellsWithOneCaptureMoves.isEmpty() && !cellsWithOneCaptureMoves.contains(startCell) && !cellsWithDoubleCaptureMoves.contains(startCell)) {
                System.out.println("Inny pionek ma dostępne bicie, wybierz właściwe pole.");

            } else {
                break;
            }
        }
    }

    public boolean getPlayerEndCell() {
        System.out.println("Podaj pole na które chcesz ruszyć");

        while (true) {
            String cellId = scanner.nextLine().toUpperCase();
            String validatedId = validateCellId(cellId);
            endCell = getCellById(validatedId);
            if (!endCell.isEmpty()) {
                return restartPlayerMove();
            }
            //If Player can make double capture and don't do the first capture, it will restart Player move
            else if (cellsWithDoubleCaptureMoves.contains(startCell) && !cellsToMoveAfterCapture(startCell).contains(endCell)) {
                return restartPlayerMove();
            }
            //If Player can make one capture and don't do it, it will restart Player move
            else if (cellsWithOneCaptureMoves.contains(startCell) && !cellsToMoveAfterCapture(startCell).contains(endCell)) {
                return restartPlayerMove();
            } else {
                moveCompleted = true;
                return true;
            }
        }
    }

    public void setupCellsParameters() {
        startRow = startCell.getRow();
        endRow = endCell.getRow();

        startColumn = startCell.getColumn();
        endColumn = endCell.getColumn();
    }

    public boolean validateDiagonalMove() {

        if (moveOneFieldForwardDiagonally()) {
            endCell.setPawn(startCell.getPawn());
            startCell.setEmpty(true);

            board.drawBoard();
            moveCompleted = true;
            return true;
        } else if (moveTwoFieldsDiagonally()) {
            Cell middleCell = getCellById(middleCellId());
            if (!(middleCell.getPawn() instanceof AIPawn)) {
                return restartPlayerMove();
            } else {
                capturePawn(middleCell);
                System.out.println("Zbiłeś pionek.");
                board.drawBoard();

                //It will loop as long as Player Pawn has possibility to capture AIPawn.
                while (!cellsToCapture(startCell).isEmpty()) {

                    System.out.println("Dokończ wielokrotne bicie. Podaj pole na które chcesz ruszyć.");
                    String validatedCellId = validateCellId(scanner.nextLine().toUpperCase());

                    endCell = getCellById(validatedCellId);
                    setupCellsParameters();

                    if (!cellsToMoveAfterCapture(startCell).contains(endCell)) {
                        System.out.println("Podałeś niewłaściwe pole.");
                    } else {
                        capturePawn(getCellById(middleCellId()));
                        System.out.println("Zbiłeś pionek.");
                        board.drawBoard();
                    }
                }
            }
            moveCompleted = true;
            return true;
        } else {
            return restartPlayerMove();
        }
    }

    public Cell getCellById(String id) {

        Optional<Cell> foundCell = board.getCells().stream()
                .filter(c -> c.getID().equals(id.toUpperCase()))
                .findFirst();

        if (foundCell.isPresent()) {
            return foundCell.get();
        }
        System.out.println("Nie ma pola o podanym ID.");
        return null;
    }

    public String validateCellId(String cellId) {

        while (true) {
            if (!cellsIDs.contains(cellId)) {
                System.out.println("Nie ma takiego pola, Wybierz poprawne pole.");
            } else {
                break;
            }
            cellId = scanner.nextLine().toUpperCase();
        }
        return cellId;
    }

    public void capturePawn(Cell cellToCapture) {
        cellToCapture.setEmpty(true);
        endCell.setPawn(startCell.getPawn());
        startCell.setEmpty(true);
        startCell = endCell;
    }

    public void findCellsWithCaptureMoves() {

        cellsWithOneCaptureMoves.clear();
        cellsWithDoubleCaptureMoves.clear();

        List<Cell> cellsWithPlayerPawns = new ArrayList<>();
        List<Cell> cells = board.getCells();

        for (Cell c : cells) {
            if (c.getPawn() instanceof PlayerPawn) {
                cellsWithPlayerPawns.add(c);
            }
        }

        for (Cell c : cellsWithPlayerPawns) {

            //List of Cells to capture by 'c' Cell.
            List<Cell> cellsToCapture = cellsToCapture(c);

            if (!cellsToCapture.isEmpty()) {
                cellsWithOneCaptureMoves.add(c);

                //cellsToMoveAfterCapture(c) returns empty Cells when moving diagonal behind AI cells, so capturing AIPawns is possible.
                for (Cell cel : cellsToMoveAfterCapture(c)) {

                    //if "empty Cell" had PlayerPawn and one move in any direction would have AIPawn, then it could make a next capture. So 'c' Cell is a cellsWithDoubleCaptureMoves.
                    if (!cellsToCapture(cel).isEmpty()) {
                        cellsWithDoubleCaptureMoves.add(c);
                        cellsWithOneCaptureMoves.remove(c);
                    }
                }
            }
        }
    }

    //Checks one Cell and two Cells in every direction diagonally from parameter Cell.
    // If there is AIPawn and behind it, is empty cell. Return List of Cells one step from parameter Cell with AIPawn.
    public List<Cell> cellsToCapture(Cell cell) {

        List<Cell> cellsToCapture = new ArrayList<>();

        Character cellRow = cell.getRow();
        int cellColumn = cell.getColumn();

        Character rowUp = (char) (cellRow - 1);
        Character rowDown = (char) (cellRow + 1);
        int columnLeft = cellColumn - 1;
        int columnRight = cellColumn + 1;


        Cell rightUpOneCellField = rightUpOneCell(rowUp, columnRight);
        Cell rightDownOneCellField = rightDownOneCell(rowDown, columnRight);
        Cell leftUpOneCellField = leftUpOneCell(rowUp, columnLeft);
        Cell leftDownOneCellField = leftDownOneCell(rowDown, columnLeft);

        Cell rightUpTwoCellsField = rightUpTwoCells(rowUp, columnRight);
        Cell rightDownTwoCellsField = rightDownTwoCells(rowDown, columnRight);
        Cell leftUpTwoCellsField = leftUpTwoCells(rowUp, columnLeft);
        Cell leftDownTwoCellsField = leftDownTwoCells(rowDown, columnLeft);

        if (isPossibleToCaptureCell(rightUpOneCellField, rightUpTwoCellsField)) {
            cellsToCapture.add(rightUpOneCellField);
        }

        if (isPossibleToCaptureCell(rightDownOneCellField, rightDownTwoCellsField)) {
            cellsToCapture.add(rightDownOneCellField);
        }

        if (isPossibleToCaptureCell(leftUpOneCellField, leftUpTwoCellsField)) {
            cellsToCapture.add(leftUpOneCellField);
        }

        if (isPossibleToCaptureCell(leftDownOneCellField, leftDownTwoCellsField)) {
            cellsToCapture.add(leftDownOneCellField);
        }

        return cellsToCapture;
    }

    //Checks one Cell and two Cells in every direction diagonally from parameter Cell.
    // If there is AIPawn and behind it is empty cell, return List of Cells two move from parameter Cell which are empty.
    //It's a list of Cells where capturing move has to finish.
    public List<Cell> cellsToMoveAfterCapture(Cell cell) {

        List<Cell> cellsToMoveAfterCapture = new ArrayList<>();

        Character cellRow = cell.getRow();
        int cellColumn = cell.getColumn();

        Character rowUp = (char) (cellRow - 1);
        Character rowDown = (char) (cellRow + 1);
        int columnLeft = cellColumn - 1;
        int columnRight = cellColumn + 1;


        Cell rightUpOneCellField = rightUpOneCell(rowUp, columnRight);
        Cell rightDownOneCellField = rightDownOneCell(rowDown, columnRight);
        Cell leftUpOneCellField = leftUpOneCell(rowUp, columnLeft);
        Cell leftDownOneCellField = leftDownOneCell(rowDown, columnLeft);

        Cell rightUpTwoCellsField = rightUpTwoCells(rowUp, columnRight);
        Cell rightDownTwoCellsField = rightDownTwoCells(rowDown, columnRight);
        Cell leftUpTwoCellsField = leftUpTwoCells(rowUp, columnLeft);
        Cell leftDownTwoCellsField = leftDownTwoCells(rowDown, columnLeft);

        if (isPossibleToCaptureCell(rightUpOneCellField, rightUpTwoCellsField)) {
            cellsToMoveAfterCapture.add(rightUpTwoCellsField);
        }

        if (isPossibleToCaptureCell(rightDownOneCellField, rightDownTwoCellsField)) {
            cellsToMoveAfterCapture.add(rightDownTwoCellsField);
        }

        if (isPossibleToCaptureCell(leftUpOneCellField, leftUpTwoCellsField)) {
            cellsToMoveAfterCapture.add(leftUpTwoCellsField);
        }

        if (isPossibleToCaptureCell(leftDownOneCellField, leftDownTwoCellsField)) {
            cellsToMoveAfterCapture.add(leftDownTwoCellsField);
        }

        return cellsToMoveAfterCapture;
    }

    public boolean isPossibleToCaptureCell(Cell oneFieldAway, Cell twoFieldsAway) {
        return oneFieldAway != null && twoFieldsAway != null && hasAIPawn(oneFieldAway) && twoFieldsAway.isEmpty();
    }

    public Cell leftDownTwoCells(Character rowDown, int columnLeft) {
        Character rowDownTwoCells = (char) (rowDown + 1);
        int columnLeftTwoCells = columnLeft - 1;

        String leftDownTwoCellsId = rowDownTwoCells.toString() + columnLeftTwoCells;
        if (cellsIDs.contains(leftDownTwoCellsId)) {
            return getCellById(leftDownTwoCellsId);

        }
        return null;
    }

    public Cell leftUpTwoCells(Character rowUp, int columnLeft) {
        Character rowUpTwoCells = (char) (rowUp - 1);
        int columnLeftTwoCells = columnLeft - 1;

        String leftUpTwoCellsId = rowUpTwoCells.toString() + columnLeftTwoCells;
        if (cellsIDs.contains(leftUpTwoCellsId)) {
            return getCellById(leftUpTwoCellsId);

        }
        return null;
    }

    public Cell rightDownTwoCells(Character rowDown, int columnRight) {
        Character rowDownTwoCells = (char) (rowDown + 1);
        int columnRightTwoCells = columnRight + 1;

        String rightDownTwoCellsId = rowDownTwoCells.toString() + columnRightTwoCells;
        if (cellsIDs.contains(rightDownTwoCellsId)) {
            return getCellById(rightDownTwoCellsId);

        }
        return null;
    }

    public Cell rightUpTwoCells(Character rowUp, int columnRight) {
        Character rowUpTwoCells = (char) (rowUp - 1);
        int columnRightTwoCells = columnRight + 1;

        String rightUpTwoCellsId = rowUpTwoCells.toString() + columnRightTwoCells;
        if (cellsIDs.contains(rightUpTwoCellsId)) {
            return getCellById(rightUpTwoCellsId);

        }
        return null;
    }

    public Cell rightUpOneCell(Character rowUp, int columnRight) {

        String rightUpCellId = rowUp.toString() + columnRight;
        if (cellsIDs.contains(rightUpCellId)) {
            return getCellById(rightUpCellId);
        }
        return null;
    }

    public Cell rightDownOneCell(Character rowDown, int columnRight) {

        String rightDownCellId = rowDown.toString() + columnRight;
        if (cellsIDs.contains(rightDownCellId)) {
            return getCellById(rightDownCellId);
        }
        return null;
    }

    public Cell leftDownOneCell(Character rowDown, int columnLeft) {

        String leftDownCellId = rowDown.toString() + columnLeft;
        if (cellsIDs.contains(leftDownCellId)) {
            return getCellById(leftDownCellId);
        }
        return null;
    }

    public Cell leftUpOneCell(Character rowUp, int columnLeft) {

        String leftUpCellId = rowUp.toString() + columnLeft;
        if (cellsIDs.contains(leftUpCellId)) {
            return getCellById(leftUpCellId);
        }
        return null;
    }

    public boolean hasPlayerPawn(Cell cell) {
        return cell.getPawn() instanceof PlayerPawn;
    }

    public boolean hasAIPawn(Cell cell) {
        return cell.getPawn() instanceof AIPawn;
    }

    public boolean moveOneFieldForwardDiagonally() {
        return startRow - endRow == 1 && Math.abs(startColumn - endColumn) == 1;
    }

    public boolean moveTwoFieldsDiagonally() {
        return Math.abs(startRow - endRow) == 2 && Math.abs(startColumn - endColumn) == 2;
    }

    public String middleCellId() {
        int middleCellRow = 0;
        int middleCellColumn = 0;

        if (isMoveForward() && isMoveRight()) {
            middleCellRow = startRow - 1;
            middleCellColumn = startColumn + 1;
        } else if (isMoveForward() && isMoveLeft()) {
            middleCellRow = startRow - 1;
            middleCellColumn = startColumn - 1;
        } else if (isMoveBackwards() && isMoveRight()) {
            middleCellRow = startRow + 1;
            middleCellColumn = startColumn + 1;
        } else {
            middleCellRow = startRow + 1;
            middleCellColumn = startColumn - 1;
        }

        return Character.toString((char) middleCellRow) + middleCellColumn;
    }


    public boolean isMoveForward() {
        return startRow - endRow > 0;
    }

    public boolean isMoveBackwards() {
        return startRow - endRow < 0;
    }

    public boolean isMoveRight() {
        return startColumn - endColumn < 0;
    }

    public boolean isMoveLeft() {
        return startColumn - endColumn > 0;
    }

    public boolean restartPlayerMove() {
        System.out.println("Nieprawidłowy ruch pionkiem. Rozpocznij ruch od nowa.");
        moveCompleted = false;
        return false;
    }


}
