import java.util.*;
import java.util.stream.Collectors;

public class AILogic {
    Scanner scanner = new Scanner(System.in);
    Random random = new Random();
    Board board;
    List<String> cellsIDs;
    Cell startCell;
    Cell endCell;
    int startRow;
    int endRow;
    int startColumn;
    int endColumn;
    List<Cell> cellsWithAIPawns;
    List<Cell> cellsWithOneMoveDiagonally = new ArrayList<>();
    List<Cell> cellsWithOneCaptureMoves = new ArrayList<>();
    List<Cell> cellsWithDoubleCaptureMoves = new ArrayList<>();


    public AILogic(Board board, List<String> cellsIDs) {
        this.board = board;
        this.cellsIDs = cellsIDs;
    }

    public void AIMove() {
        System.out.println("Komputer rozpoczyna ruch");

        cellsWithAIPawns = findAllCellsWithAIPAwns();
        getCellsWithOneMoveDiagonally(cellsWithAIPawns);


        findCellsWithCaptureMoves();

        makeMove();
        board.drawBoard();

        System.out.println("Komputer kończy ruch");
    }

    public void makeMove() {

        //Find randomly one Cell with double capturing move and capture Player Pawn as long there is capturing possibility.
        if (!cellsWithDoubleCaptureMoves.isEmpty()) {

            //Find Cell to start from, with double capturing possibility.
            startCell = setUpStartCellWithDoubleCaptureMove();
            //Set up End Cell which has possibility to capture Player Pawn in next move
            endCell = setUpEndCellWithDoubleCaptureMove();

            setupCellsParameters();
            Cell cellToCaptureOver = getCellById(findMiddleCellID());

            capturePawn(cellToCaptureOver);
            System.out.println("Komputer zbił pionek na polu: " + cellToCaptureOver.getID() + " i wykonał ruch na pole: " + endCell.getID());

            //AI will capture Player Pawns, as long as there is possibility to capture.
            while (!cellsToCapture(startCell).isEmpty()) {

                //Find Cell where capture Pawn can end.
                endCell = setUpEndCellWithOneCaptureMove();

                setupCellsParameters();

                Cell cellToCaptureOver2 = getCellById(findMiddleCellID());

                //If Player Pawn is captured, endCell is changed for startCell.
                capturePawn(cellToCaptureOver2);
                System.out.println("Komputer zbił pionek na polu: " + cellToCaptureOver2.getID() + " i wykonał ruch na pole: " + endCell.getID());

            }

        }
        //Find randomly one Cell with one capture move and capture Player Pawn.
        else if (!cellsWithOneCaptureMoves.isEmpty()) {

            //Find Cell to start move from
            startCell = setUpStarCellWithOneCaptureMove();

            //Find Cell where to move
            endCell = setUpEndCellWithOneCaptureMove();

            setupCellsParameters();

            Cell cellToCaptureOver = getCellById(findMiddleCellID());
            capturePawn(cellToCaptureOver);

            System.out.println("Komputer zbił pionek na polu: " + cellToCaptureOver.getID() + ", ruch zakończył na polu: " + endCell.getID());

        }
        //Find randomly one Cell without capturing possibility and make one-step move diagonally.
        else {
            //Get random Cell with Pawn which can move one field diagonally.
            startCell = getRandomCellToStartMove();

            //Get random Cell where Pawn from startCell can end move.
            endCell = getRandomCellToEndMove();

            endCell.setPawn(startCell.getPawn());
            startCell.setEmpty(true);

            System.out.println("Komputer wykonał ruch z pola: " + startCell.getID() + ", na pole: " + endCell.getID() + ".");
        }
    }

    public Cell getRandomCellToStartMove() {
        if(cellsWithOneMoveDiagonally.isEmpty()){
            System.out.println("Komputer nie ma możliwości ruchu, wygrałeś!!!!!!!");
            System.exit(0);
        }
        int randomIndex = random.nextInt(0, cellsWithOneMoveDiagonally.size());
        return cellsWithOneMoveDiagonally.get(randomIndex);
    }

    public Cell getRandomCellToEndMove() {
        List<Cell> cellsToMakeMoveOn = possibleOneStepMovesForSelectedCell(startCell);

        int randomIndex2 = random.nextInt(0, cellsToMakeMoveOn.size());

        return cellsToMakeMoveOn.get(randomIndex2);
    }

    public void getCellsWithOneMoveDiagonally(List<Cell> cellsWithAIPawns) {
        cellsWithOneMoveDiagonally.clear();

        for (Cell cell : cellsWithAIPawns) {
            if (!possibleOneStepMovesForSelectedCell(cell).isEmpty()) {
                cellsWithOneMoveDiagonally.add(cell);
            }
        }
    }

    //Find empty Cells one step diagonally down the board.
    public List<Cell> possibleOneStepMovesForSelectedCell(Cell cell) {

        List<Cell> emptyCellsToMove = new ArrayList<>();
        char startRow = cell.getRow();
        int startColumn = cell.getColumn();

        char endRow = (char) (startRow + 1);
        int endColumn1 = startColumn + 1;
        int endColumn2 = startColumn - 1;

        String firstCellId = Character.toString(endRow) + endColumn1;
        String secondCellId = Character.toString(endRow) + endColumn2;

        Cell firstCell = getCellById(firstCellId);
        Cell secondCell = getCellById(secondCellId);

        if (isCellIdOnBoard(firstCellId) && firstCell.isEmpty())
            emptyCellsToMove.add(firstCell);
        if (isCellIdOnBoard(secondCellId) && secondCell.isEmpty()) {
            emptyCellsToMove.add(secondCell);
        }

        return emptyCellsToMove;
    }

    public Cell setUpStartCellWithDoubleCaptureMove() {
        int randomIndex = random.nextInt(0, cellsWithDoubleCaptureMoves.size());
        return cellsWithDoubleCaptureMoves.get(randomIndex);
    }

    public Cell setUpStarCellWithOneCaptureMove() {
        int randomIndex = random.nextInt(0, cellsWithOneCaptureMoves.size());
        return cellsWithOneCaptureMoves.get(randomIndex);
    }

    public Cell setUpEndCellWithOneCaptureMove() {
        List<Cell> cellsToEndMove = cellsToMoveAfterCapture(startCell);
        int randomIndex = random.nextInt(0, cellsToEndMove.size());
        return cellsToEndMove.get(randomIndex);
    }

    public Cell setUpEndCellWithDoubleCaptureMove() {
        List<Cell> cellsToEndMove = cellsToMoveAfterCapture(startCell);

        cellsToEndMove.removeIf(c -> cellsToCapture(c).isEmpty());
        int randomIndex = random.nextInt(0, cellsToEndMove.size());
        return cellsToEndMove.get(randomIndex);
    }

    public void capturePawn(Cell cellToCapture) {
        cellToCapture.setEmpty(true);
        endCell.setPawn(startCell.getPawn());
        startCell.setEmpty(true);
        startCell = endCell;
    }

    public List<Cell> findAllCellsWithAIPAwns() {
        return board.getCells().stream()
                .filter(c -> c.getPawn() instanceof AIPawn)
                .collect(Collectors.toList());
    }

    public String findMiddleCellID() {
        char middleCellRow;
        int middleCellColumn;

        if (startRow - endRow > 0) {
            middleCellRow = (char) (startRow - 1);
        } else {
            middleCellRow = (char) (startRow + 1);
        }

        if (startColumn - endColumn > 0) {
            middleCellColumn = startColumn - 1;
        } else {
            middleCellColumn = startColumn + 1;
        }

        return Character.toString(middleCellRow) + middleCellColumn;
    }

    public void setupCellsParameters() {
        startRow = startCell.getRow();
        endRow = endCell.getRow();

        startColumn = startCell.getColumn();
        endColumn = endCell.getColumn();

    }

    public void findCellsWithCaptureMoves() {

        cellsWithOneCaptureMoves.clear();
        cellsWithDoubleCaptureMoves.clear();

        List<Cell> cellsWithAIPawns = new ArrayList<>();
        List<Cell> cells = board.getCells();

        for (Cell c : cells) {
            if (c.getPawn() instanceof AIPawn) {
                cellsWithAIPawns.add(c);
            }
        }

        for (Cell c : cellsWithAIPawns) {

            //List of Cells to capture by 'c' Cell.
            List<Cell> cellsToCapture = cellsToCapture(c);

            if (!cellsToCapture.isEmpty()) {
                cellsWithOneCaptureMoves.add(c);

                //cellsToMoveAfterCapture(c) returns empty Cells when moving diagonal behind Player cells, so capturing PlayerPawns is possible.
                for (Cell cel : cellsToMoveAfterCapture(c)) {

                    //if "empty Cell" had AIPawn and one Cell in any direction would have PlayerPawn, then it could make a next capture. So 'c' Cell is a cellsWithDoubleCaptureMoves.
                    if (!cellsToCapture(cel).isEmpty()) {
                        cellsWithDoubleCaptureMoves.add(c);
                        cellsWithOneCaptureMoves.remove(c);
                    }

                }
            }
        }
    }

    public List<Cell> cellsToCapture(Cell cell) {

        List<Cell> cellsToCapture = new ArrayList<>();

        Character cellRow = cell.getRow();
        int cellColumn = cell.getColumn();

        Character rowUp = (char) (cellRow - 1);
        Character rowDown = (char) (cellRow + 1);
        int columnLeft = cellColumn - 1;
        int columnRight = cellColumn + 1;


        Cell rightUpOneCellField = rightUpOneCell(rowUp, columnRight);
        Cell rightDownOneCellField = rightDownOneCell(rowDown, columnRight);
        Cell leftUpOneCellField = leftUpOneCell(rowUp, columnLeft);
        Cell leftDownOneCellField = leftDownOneCell(rowDown, columnLeft);

        Cell rightUpTwoCellsField = rightUpTwoCells(rowUp, columnRight);
        Cell rightDownTwoCellsField = rightDownTwoCells(rowDown, columnRight);
        Cell leftUpTwoCellsField = leftUpTwoCells(rowUp, columnLeft);
        Cell leftDownTwoCellsField = leftDownTwoCells(rowDown, columnLeft);

        if (isPossibleToCaptureCell(rightUpOneCellField, rightUpTwoCellsField)) {
            cellsToCapture.add(rightUpOneCellField);
        }

        if (isPossibleToCaptureCell(rightDownOneCellField, rightDownTwoCellsField)) {
            cellsToCapture.add(rightDownOneCellField);
        }

        if (isPossibleToCaptureCell(leftUpOneCellField, leftUpTwoCellsField)) {
            cellsToCapture.add(leftUpOneCellField);
        }

        if (isPossibleToCaptureCell(leftDownOneCellField, leftDownTwoCellsField)) {
            cellsToCapture.add(leftDownOneCellField);
        }

        return cellsToCapture;
    }

    public List<Cell> cellsToMoveAfterCapture(Cell cell) {

        List<Cell> cellsToMoveAfterCapture = new ArrayList<>();

        Character cellRow = cell.getRow();
        int cellColumn = cell.getColumn();

        Character rowUp = (char) (cellRow - 1);
        Character rowDown = (char) (cellRow + 1);
        int columnLeft = cellColumn - 1;
        int columnRight = cellColumn + 1;


        Cell rightUpOneCellField = rightUpOneCell(rowUp, columnRight);
        Cell rightDownOneCellField = rightDownOneCell(rowDown, columnRight);
        Cell leftUpOneCellField = leftUpOneCell(rowUp, columnLeft);
        Cell leftDownOneCellField = leftDownOneCell(rowDown, columnLeft);

        Cell rightUpTwoCellsField = rightUpTwoCells(rowUp, columnRight);
        Cell rightDownTwoCellsField = rightDownTwoCells(rowDown, columnRight);
        Cell leftUpTwoCellsField = leftUpTwoCells(rowUp, columnLeft);
        Cell leftDownTwoCellsField = leftDownTwoCells(rowDown, columnLeft);

        if (isPossibleToCaptureCell(rightUpOneCellField, rightUpTwoCellsField)) {
            cellsToMoveAfterCapture.add(rightUpTwoCellsField);
        }

        if (isPossibleToCaptureCell(rightDownOneCellField, rightDownTwoCellsField)) {
            cellsToMoveAfterCapture.add(rightDownTwoCellsField);
        }

        if (isPossibleToCaptureCell(leftUpOneCellField, leftUpTwoCellsField)) {
            cellsToMoveAfterCapture.add(leftUpTwoCellsField);
        }

        if (isPossibleToCaptureCell(leftDownOneCellField, leftDownTwoCellsField)) {
            cellsToMoveAfterCapture.add(leftDownTwoCellsField);
        }

        return cellsToMoveAfterCapture;
    }

    public boolean isPossibleToCaptureCell(Cell oneFieldAway, Cell twoFieldsAway) {
        return oneFieldAway != null && twoFieldsAway != null && hasPlayerPawn(oneFieldAway) && twoFieldsAway.isEmpty();
    }

    public boolean hasPlayerPawn(Cell cell) {
        return cell.getPawn() instanceof PlayerPawn;
    }

    public Cell leftDownTwoCells(Character rowDown, int columnLeft) {
        Character rowDownTwoCells = (char) (rowDown + 1);
        int columnLeftTwoCells = columnLeft - 1;

        String leftDownTwoCellsId = rowDownTwoCells.toString() + columnLeftTwoCells;
        if (cellsIDs.contains(leftDownTwoCellsId)) {
            return getCellById(leftDownTwoCellsId);

        }
        return null;
    }

    public Cell leftUpTwoCells(Character rowUp, int columnLeft) {
        Character rowUpTwoCells = (char) (rowUp - 1);
        int columnLeftTwoCells = columnLeft - 1;

        String leftUpTwoCellsId = rowUpTwoCells.toString() + columnLeftTwoCells;
        if (cellsIDs.contains(leftUpTwoCellsId)) {
            return getCellById(leftUpTwoCellsId);

        }
        return null;
    }

    public Cell rightDownTwoCells(Character rowDown, int columnRight) {
        Character rowDownTwoCells = (char) (rowDown + 1);
        int columnRightTwoCells = columnRight + 1;

        String rightDownTwoCellsId = rowDownTwoCells.toString() + columnRightTwoCells;
        if (cellsIDs.contains(rightDownTwoCellsId)) {
            return getCellById(rightDownTwoCellsId);

        }
        return null;
    }

    public Cell rightUpTwoCells(Character rowUp, int columnRight) {
        Character rowUpTwoCells = (char) (rowUp - 1);
        int columnRightTwoCells = columnRight + 1;

        String rightUpTwoCellsId = rowUpTwoCells.toString() + columnRightTwoCells;
        if (cellsIDs.contains(rightUpTwoCellsId)) {
            return getCellById(rightUpTwoCellsId);

        }
        return null;
    }

    public Cell rightUpOneCell(Character rowUp, int columnRight) {

        String rightUpCellId = rowUp.toString() + columnRight;
        if (cellsIDs.contains(rightUpCellId)) {
            return getCellById(rightUpCellId);
        }
        return null;
    }

    public Cell rightDownOneCell(Character rowDown, int columnRight) {

        String rightDownCellId = rowDown.toString() + columnRight;
        if (cellsIDs.contains(rightDownCellId)) {
            return getCellById(rightDownCellId);
        }
        return null;
    }

    public Cell leftDownOneCell(Character rowDown, int columnLeft) {

        String leftDownCellId = rowDown.toString() + columnLeft;
        if (cellsIDs.contains(leftDownCellId)) {
            return getCellById(leftDownCellId);
        }
        return null;
    }

    public Cell leftUpOneCell(Character rowUp, int columnLeft) {

        String leftUpCellId = rowUp.toString() + columnLeft;
        if (cellsIDs.contains(leftUpCellId)) {
            return getCellById(leftUpCellId);
        }
        return null;
    }

    public Cell getCellById(String id) {

        Optional<Cell> foundCell = board.getCells().stream()
                .filter(c -> c.getID().equals(id.toUpperCase()))
                .findFirst();

        return foundCell.orElse(null);
    }

    public boolean isCellIdOnBoard(String cellId) {

        return cellsIDs.contains(cellId.toUpperCase());
    }
}
