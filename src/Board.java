import java.util.ArrayList;
import java.util.List;

public class Board {
    private final char firstLetter;
    private final char lastLetter;
    private final int rowLength;
    private final int pawnsQuantity;
    private final int pawnsInRow;
    private final Character[] rowLetters;
    private final List<Cell> cells;


    public Board(char firstLetter, char lastLetter, int rowLength, int pawnsQuantity, int pawnsInRow) {
        this.firstLetter = firstLetter;
        this.lastLetter = lastLetter;
        this.rowLength = rowLength;
        this.pawnsQuantity = pawnsQuantity;
        this.pawnsInRow = pawnsInRow;
        this.rowLetters = setLettersArray();
        this.cells = setBoardWithPawns();
    }

    public List<Cell> getCells() {
        return cells;
    }

    private  Character[] setLettersArray(){
        int arrayLength = lastLetter - firstLetter + 1;
        Character[] charArray = new Character[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            charArray[i] = (char) (firstLetter + i);
        }
        return charArray;
    }

    public List<Cell> setBoardWithPawns(){
        List<Cell> emptyBoard = makeEmptyCells();
        List<Cell> boardWithPlayerPawns = addPlayerPawns(emptyBoard);

        return addAIPawns(boardWithPlayerPawns);
    }
    public List<Cell> makeEmptyCells() {

        List<Cell> cells = new ArrayList<>();
        int counter = 0;

        for (int i = 1; i <= rowLength; i++) {
            if (counter == rowLetters.length) {
                break;
            }
            cells.add(new Cell(rowLetters[counter], i));
            if (i == rowLength) {
                counter++;
                i = 0;
            }
        }
        return cells;
    }

    public List<Cell> addPlayerPawns(List<Cell> cells) {
        int counter = 0;
        int counter2 = 1;

        int max = cells.size() - 1;
        int min = max - pawnsQuantity * 2;

        for (int i = max; i > min; i -= 2) {
            cells.get(i).setPawn(new PlayerPawn());
            //cells.get(i).setEmpty(false);
            counter++;
            if (counter == pawnsInRow * counter2 && counter2 % 2 == 1) {
                i -= 1;
                counter2++;
            }
            if (counter == pawnsInRow * counter2 && counter2 % 2 == 0) {
                i += 1;
                counter2++;
            }
        }
        return cells;
    }

    public List<Cell> addAIPawns(List<Cell> cells) {
        int counter = 0;
        int counter2 = 1;

        for (int i = 0; i < pawnsQuantity * 2; i += 2) {
            cells.get(i).setPawn(new AIPawn());
            //cells.get(i).setEmpty(false);
            counter++;

            if (counter == pawnsInRow * counter2 && counter2 % 2 == 1) {
                i += 1;
                counter2++;
            }
            if (counter == pawnsInRow * counter2 && counter2 % 2 == 0) {
                i -= 1;
                counter2++;
            }
        }
        return cells;
    }

    public void drawBoard() {
        System.out.print("    ");

        // Write first line with numbers.
        for (int i = 0; i < rowLength; i++) {
            if (i == rowLength - 1) {
                System.out.println(i + 1);
            } else {
                System.out.print(i + 1 + "  ");
            }
        }

        //Iterate through each letter form rowLetters[], with row as long as rowLength and return Pawn marker for each cell.
        int counter = rowLength;
        int counter2 = 0;
        for (int i = 0; i < rowLetters.length; i++) {
            System.out.print(rowLetters[i] + "   ");
            for (int j = counter2; j < counter; j++) {
                if (j == counter - 1) {
                    System.out.println(cells.get(j).getPawn().getMarker());
                    counter2 += rowLength;
                } else {
                    System.out.print(cells.get(j).getPawn().getMarker() + "  ");
                }
            }
            counter += rowLength;
        }
    }
}
