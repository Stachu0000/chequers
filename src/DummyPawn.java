public class DummyPawn extends Pawn{
    private final String marker = "_";

    @Override
    public String getMarker() {
        return marker;
    }
}
