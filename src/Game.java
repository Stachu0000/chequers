import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {

    Scanner scanner = new Scanner(System.in);
    Board board = new Board('A', 'H', 8, 12, 4);
    List<String> cellsIDs = new ArrayList<>();

    public void playGame() {
        board.getCells().forEach(c -> cellsIDs.add(c.getID()));


        PlayerLogic playerLogic = new PlayerLogic(board, cellsIDs);

        AILogic aiLogic = new AILogic(board, cellsIDs);

        board.drawBoard();
        while (true) {

            if (!playerHasPawns()) {
                System.out.println("Komputer wygrał!!!!!!!!!!!!!!!!!!!!! Skynet też zaczynał od warcab.");
                break;
            }

            playerLogic.playerMove();

            if (!aIHasPawns()) {
                System.out.println("Wygrałeś!!!!!!!!!!!!!!!!!!!!!!!!!! AI nigdy nie dorówna potędze ludzkiego umysłu :)");
                break;
            }
            aiLogic.AIMove();
        }
    }

    public boolean playerHasPawns() {
        return board.getCells().stream()
                .anyMatch(c -> c.getPawn() instanceof PlayerPawn);
    }

    public boolean aIHasPawns() {
        return board.getCells().stream()
                .anyMatch(c -> c.getPawn() instanceof AIPawn);
    }
}
