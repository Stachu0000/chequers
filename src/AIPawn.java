public class AIPawn extends Pawn{
    private final String marker = "X";

    @Override
    public String getMarker() {
        return marker;
    }
}
