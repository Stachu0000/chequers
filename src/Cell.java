import java.util.ArrayList;
import java.util.List;

public class Cell {
    private String iD;
    private Character row;
    private int column;
    private Pawn pawn;
    private boolean isEmpty;

    public Cell(Character row, int column) {
        this.row = row;
        this.column = column;
        this.iD = row.toString() + column;
        this.isEmpty = true;
        this.pawn = new DummyPawn();
    }

    public String getID() {
        return iD;
    }

    public Character getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Pawn getPawn() {
        return pawn;
    }

    public void setPawn(Pawn pawn) {
        this.pawn = pawn;
        this.isEmpty = false;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        if (empty) {
            isEmpty = true;
            pawn = new DummyPawn();
        } else {
            isEmpty = false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(getClass() != obj.getClass()){
            return false;
        }
        Cell cell = (Cell) obj;
        return iD.equals(cell.iD);
    }

    @Override
    public int hashCode() {
        return iD.hashCode();
    }
}
