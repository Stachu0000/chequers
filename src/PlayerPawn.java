public class PlayerPawn extends Pawn{
    private final String marker = "O";

    @Override
    public String getMarker() {
        return marker;
    }
}
